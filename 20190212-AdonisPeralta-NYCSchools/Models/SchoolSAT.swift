//
//  SchoolSAT.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import Foundation

struct SchoolSAT: Codable {
    let dbnId: String
    let schoolName: String

    let numberOfSATTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String

    enum CodingKeys: String, CodingKey {
        case dbnId = "dbn"
        case schoolName = "school_name"

        case numberOfSATTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
