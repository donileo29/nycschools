//
//  School.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import Foundation

struct School: Codable {
    // School has a lot more fields than these but no need to specify them here if we dont plan to use them in our
    // model - that would just waste memory.
    let dbnId: String
    let schoolName: String
    let city: String

    // Borough may be missing so lets make it optional
    let borough: String?

    enum CodingKeys: String, CodingKey {
        case dbnId = "dbn"
        case schoolName = "school_name"
        case borough, city
    }
}
