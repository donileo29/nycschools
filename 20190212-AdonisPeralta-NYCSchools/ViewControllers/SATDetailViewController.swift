//
//  SATDetailViewController.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import UIKit

class SATDetailViewController: UIViewController {
    private let schoolDbn: String
    private var schoolSATInfo: SchoolSAT? {
        didSet {
            DispatchQueue.main.async { self.configureInfoLabel() }
        }
    }

    private lazy var infoLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()

    init(schoolDbn: String, schoolName: String) {
        self.schoolDbn = schoolDbn
        super.init(nibName: nil, bundle: nil)
        title = schoolName
    }

    required init?(coder aDecoder: NSCoder) {
        // Here we opt to not truly implement this method so we explicitly opt out of using Storyboard initialization of
        // this view controller. At the same time this may prevent archiving/encoding of this view controller for state
        // restoration for example but since we are not doing that lets not worry about that for now.
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupDesign()
        layoutViews()
        fetchData()
    }

    private func setupDesign() {
        view.backgroundColor = .white
    }

    private func layoutViews() {
        view.addSubview(infoLabel)
        infoLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            infoLabel.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor, constant: 25),
            infoLabel.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant: -25),
            infoLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            infoLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    private func fetchData() {
        SchoolsAPI.shared.fetchSATInfo(forSchoolWithDbn: schoolDbn) { [weak self] schoolsSAT, err in
            guard err == nil else {
                // Here we opt to display a friendly message to the user for the error instead of something
                // overly verbose. Ideally we as the developer would implement some sort of mechanism here
                // so that user can  can send us logs via email.
                self?.presentAlert(title: "Error",
                                   message: "Error occurred while fetching data, please contact the developer")
                return
            }

            self?.schoolSATInfo = schoolsSAT
        }
    }

    private func configureInfoLabel() {
        guard let info = schoolSATInfo else {
            // If we get called by the API with no info (nil value for schoolSATInfo) lets display the no
            // results state to the user
            infoLabel.text = "No SAT Data found for this school"
            return
        }

        infoLabel.text = """
                         Number of Test Takers: \(info.numberOfSATTestTakers)
                         Math Score Avg: \(info.satMathAvgScore)
                         Critical Reading Score Avg: \(info.satCriticalReadingAvgScore)
                         Writing Score Avg: \(info.satWritingAvgScore)
                         """
    }
}
