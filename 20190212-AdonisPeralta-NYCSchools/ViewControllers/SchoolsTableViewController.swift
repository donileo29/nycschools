//
//  SchoolsTableViewController.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
    private let cellReuseIdentifier = "schoolCell"

    private var lastFetchedPageOffset = 0
    private var schoolsAllPaged = false
    private var shouldShowLoadingCell = false

    private var schools: [School]?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        setupDesign()

        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshSchools), for: .valueChanged)

        refreshControl?.beginRefreshing()
        refreshSchools()
    }

    private func setupTableView() {
        tableView?.register(SchoolTableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }

    private func setupDesign() {
        title = "NYC Schools: SAT Data"
        clearsSelectionOnViewWillAppear = true
    }

    @objc private func refreshSchools() {
        lastFetchedPageOffset = 0
        schoolsAllPaged = false

        // A refresh resets all our current existing data which is different then what paging does in
        // appending new data to show to our user.
        schools = nil

        fetchData()
    }

    /// Fetches School API data by paging. Every single call to this method appends a max of 30 school elements
    /// to our main dataSource array (schools). Once called enough times that there is no more additional data this
    /// method does nothing and sets the schoolsAllPaged boolean to true.
    private func fetchData() {
        // If schoolsAllPaged is set we are done as we have paged all the data
        guard !schoolsAllPaged else {
            refreshControl?.endRefreshing()
            return
        }

        // Guard against our lastFetchedPageOffset being incorrectly set
        guard lastFetchedPageOffset > -1 else {
            preconditionFailure(
                "lastFetchedPageOffset must be positive, there must be a programmer error here somewhere"
            )
        }

        // The Socrata API seems to not return the number of pages total on each API call we make so
        // here we opt to continously make calls to it until it specifically returns to us an Empty array of schools.
        // At that point we are done paging our results and should not make any more calls.
        SchoolsAPI.shared.fetchSchools(limit: 25, offset: lastFetchedPageOffset) {
            [weak self] schools, err in

            defer { DispatchQueue.main.async { self?.refreshControl?.endRefreshing() } }

            // Lets unwrap self here so that we dont have to do optional chaning such as self?.methodName below
            guard let self = self else { return }

            guard let schools = schools, err == nil else {
                // Here we opt to display a friendly message to the user for the error instead of something
                // overly verbose. Ideally we as the developer would implement some sort of mechanism here
                // so that user can  can send us logs via email.
                self.presentAlert(title: "Error",
                             message: "An error occurred while fetching data, please contact the developer")
                return
            }

            if schools.count > 0 {
                // Maintain the state of the last fetched paged offset so we can continue where we last left off
                self.lastFetchedPageOffset += schools.count

                // Populate our dataSource array with the api results only when the amount of schools return is greater
                // than 0. If our datasource was already populated it means we had paged before so now we just append
                // the results.
                self.schools == nil ? self.schools = schools : self.schools?.append(contentsOf: schools)
                self.shouldShowLoadingCell = true
            } else {
                // Empty result set returned so no more data available to page
                self.schoolsAllPaged = true
                self.shouldShowLoadingCell = false
            }

            DispatchQueue.main.async { self.tableView?.reloadData() }
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard schools != nil else { return  0 }
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let schools = schools else { return  0 }

        // Lets make room to show a loading cell so that the user can page the results of the tableView by scrolling.
        return shouldShowLoadingCell ? schools.count + 1 : schools.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let isLoadingIndexPath = isLoadingCellIndexPath(indexPath) else { return UITableViewCell() }

        guard !isLoadingIndexPath else {
            // If the indexPath row we are being asked to show belongs to the loading cell lets show the
            // the loading cell with the activity indicator on it to let the user know we are fetching more
            // results for them.
            return LoadingTableViewCell(style: .`default`, reuseIdentifier: "loadingCell")
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)

        guard let schools = schools, schools.count > 0 else {
            // Here, in the case that our schools returns an empty array (0 elements) we could choose to create a
            // custom subclass of UITableViewCell that allows us to return a cell with a "no results view"
            // that tells the user the API call succeeded but there were 0 schools returned in the result.
            return cell
        }

        let school = schools[indexPath.row]
        cell.textLabel?.text = school.schoolName

        var detailText = "City: \(school.city)"
        if let borough = school.borough { detailText += ", Borough: \(borough)"}

        cell.detailTextLabel?.text = detailText
        return cell
    }

    //MARK: - TableViewDelegate Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let school = schools?[indexPath.row] else { return }

        let schoolDbn = school.dbnId
        let satInfoViewController = SATDetailViewController(schoolDbn: schoolDbn, schoolName: school.schoolName)
        navigationController?.pushViewController(satInfoViewController, animated: true)
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingCellIndexPath(indexPath) == true else { return }

        // If tableView is just about to being showing our "loading" cell lets start the call to fetch more results
        // Which itself will reload our tableView with our new data resulting from the call.
        fetchData()
    }

    private func isLoadingCellIndexPath(_ indexPath: IndexPath) -> Bool? {
        guard let schools = schools else { return nil }
        guard shouldShowLoadingCell else { return false }

        return indexPath.row == schools.count
    }
}
