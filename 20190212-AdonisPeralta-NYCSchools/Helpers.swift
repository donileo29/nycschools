//
//  Helpers.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func presentAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
            self?.dismiss(animated: true)
        }

        alertController.addAction(okAction)

        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
}
