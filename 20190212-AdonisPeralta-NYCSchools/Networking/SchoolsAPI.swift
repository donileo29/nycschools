//
//  SchoolsAPI.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import Foundation

enum APIError: Error {
    case httpError(message: String)
    case noData(message: String)
    case jsonDecodeError(message: String)
}

class SchoolsAPI {
    static let shared = SchoolsAPI()

    func fetchSchools(limit: Int, offset: Int, completion: @escaping ([School]?, Error?) -> Void) {
        guard var urlComponents = URLComponents(string: Constants.APIUrls.schoolsDataUrl) else {
            preconditionFailure(ErrorMessageStrings.invalidSchoolsDataUrl)
        }

        urlComponents.queryItems = [
            URLQueryItem(name: "$limit", value: String(limit)),
            URLQueryItem(name: "$offset", value: String(offset)),
            // Socrata doesn't guarantee implicit ordering of results for paging so lets tell them to enable
            // ordering.
            URLQueryItem(name: "$order", value: ":id"),
        ]

        guard let request = urlComponents.url else {
            preconditionFailure(ErrorMessageStrings.invalidHttpQueryReqParams)
        }

        URLSession.shared.dataTask(with: request) { data, resp, err in
            guard err == nil else {
                completion(nil, err)
                return
            }

            guard let data = data else {
                completion(nil, APIError.noData(message: ErrorMessageStrings.noDataMsg))
                return
            }

            guard let httpResp = resp as? HTTPURLResponse else {
                completion(nil, APIError.httpError(message: ErrorMessageStrings.invalidHttpResp))
                return
            }

            guard (200...299).contains(httpResp.statusCode) else {
                let httpErrorMsg = HTTPURLResponse.localizedString(forStatusCode: httpResp.statusCode)
                let errMessage = "Internal server error: \(httpErrorMsg)"

                completion(nil, APIError.httpError(message: errMessage))
                return
            }

            do {
                let schools = try JSONDecoder().decode([School].self, from: data)
                completion(schools, nil)
            } catch {
                let errMessage = "Error decoding server returned json data: \(error.localizedDescription)"
                completion(nil, APIError.jsonDecodeError(message: errMessage))
            }
        }.resume()
    }

    func fetchSATInfo(forSchoolWithDbn dbn: String, withCompletion completion: @escaping (SchoolSAT?, Error?) -> Void) {
        guard var urlComponents = URLComponents(string: Constants.APIUrls.schoolsSATDataUrl) else {
            preconditionFailure(ErrorMessageStrings.invalidSchoolsSATUrl)
        }

        // Lets pass a school dbn parameter here so the API will only give us the SAT information for that specific
        // school
        urlComponents.queryItems = [ URLQueryItem(name: "dbn", value: dbn) ]

        guard let request = urlComponents.url else {
            preconditionFailure(ErrorMessageStrings.invalidHttpQueryReqParams)
        }

        URLSession.shared.dataTask(with: request) { data, resp, err in
            guard err == nil else {
                completion(nil, err)
                return
            }

            guard let data = data else {
                completion(nil, APIError.noData(message: ErrorMessageStrings.noDataMsg))
                return
            }

            guard let httpResp = resp as? HTTPURLResponse else {
                completion(nil, APIError.httpError(message: ErrorMessageStrings.invalidHttpResp))
                return
            }

            guard (200...299).contains(httpResp.statusCode) else {
                let httpErrorMsg = HTTPURLResponse.localizedString(forStatusCode: httpResp.statusCode)
                let errMessage = "Internal server error: \(httpErrorMsg)"

                completion(nil, APIError.httpError(message: errMessage))
                return
            }

            do {
                let schoolsSAT = try JSONDecoder().decode([SchoolSAT].self, from: data)
                guard let schoolSAT = schoolsSAT.first else {
                    // If no data returned from the API for this query we signify that there was no data
                    // but an error DID NOT occur.
                    completion(nil, nil)
                    return
                }
                completion(schoolSAT, nil)
            } catch {
                let errMessage = "Error decoding server returned json data: \(error.localizedDescription)"
                completion(nil, APIError.jsonDecodeError(message: errMessage))
            }
            }.resume()
    }
}

private struct ErrorMessageStrings {
    static let invalidSchoolsDataUrl = "The primary schools data url stored in Constants.APIUrls.schoolsDataUrl" +
    "seems invalid. Please make sure it is correct."

    static let invalidSchoolsSATUrl = "The schools SAT data url stored in Constants.APIUrls.schoolsSATDataUrl seems"
        + "invalid. Please make sure it is correct."

    static let noDataMsg = "API call was successful but API returned no data please make sure you are making the" +
    "correct API request."

    static let invalidHttpResp = "Server response is not a valid http url response"
    static let invalidHttpQueryReqParams = "Request params seem to be invalid, please inspect and fix."
}
