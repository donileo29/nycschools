//
//  Constants.swift
//  NYCSchools
//
//  Created by Adonis Peralta on 2/12/19.
//  Copyright © 2019 Adonis Peralta. All rights reserved.
//

import Foundation

struct Constants {
    struct APIUrls {
        static let schoolsDataUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let schoolsSATDataUrl =  "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
}
